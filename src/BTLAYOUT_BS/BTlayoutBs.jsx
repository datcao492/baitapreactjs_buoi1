import React, { Component } from "react";
import Banner from "./Banner";
import Footer from "./Footer";
import Header from "./Header";
import Items from "./Items";

export default class BTlayoutBs extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container">
          <Banner />

          <div className="row">
            <Items />
            <Items />
            <Items />
            <Items />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
