import logo from "./logo.svg";
import "./App.css";
import BTlayoutBs from "./BTLAYOUT_BS/BTlayoutBs";

function App() {
  return (
    <div className="App">
      <BTlayoutBs />
    </div>
  );
}

export default App;
